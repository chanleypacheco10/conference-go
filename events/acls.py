import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = f'https://api.pexels.com/v1/search?query={city}%20{state}&per_page=1'
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    content = json.loads(response.text)
    first_photo = content["photos"][0]
    return first_photo


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geocoding_url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}'
    geocoding_response = requests.get(geocoding_url)
    geo_data = geocoding_response.json()

    latitude = geo_data[0]['lat']
    longitude = geo_data[0]['lon']

    # Create the URL for the current weather
    # API with the latitude and longitude
    weather_url = f'https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}'
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()

    temp_kelvin = weather_data['main']['temp']
    weather_description = weather_data['weather'][0]['description']

    # Convert temperature to Celsius and Fahrenheit
    temp_celsius = temp_kelvin - 273.15
    temp_fahrenheit = (temp_kelvin - 273.15) * 9/5 + 32

    return {
        'temperature_celsius': round(temp_celsius),
        'temperature_fahrenheit': round(temp_fahrenheit),
        'description': weather_description
    }
